﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovesText : MonoBehaviour
{

    private Text text;

    private void Awake()
    {
        text = GetComponentInChildren<Text>();
    }

    public void setText(int moves)
    {
        text.text = "MOVES: " + moves.ToString();
    }
}
