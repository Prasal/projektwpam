﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarkerOfControlForPlayer : MonoBehaviour {

	private Game game;
	private Image image;
	
	void Start () {
		game = FindObjectOfType<Game>();
		image = GetComponentInChildren<Image>();
	}
	
	void Update ()
	{
		image.enabled = game.isScreenLocked;
	}
}
