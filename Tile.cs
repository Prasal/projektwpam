﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tile : MonoBehaviour {

    public int row;
    public int col;
    public string letter;
    public bool active;
    public Color32 color;
    private Text tileText;
    private Image tileImage;

    public string Letter{
        get{
            return letter;
        }
    
        set{
            letter = value;
        }
    }

    void Awake(){
        tileText = GetComponentInChildren<Text>();
        tileImage = transform.Find("TileWithLetter").GetComponent<Image>();
    }

    void Update () {
        tileText.text = letter;
        tileImage.color = color;
    }
    public bool isItYou(float x, float y){
        Vector3 vec = tileImage.transform.position;
        Rect rect = tileImage.rectTransform.rect;

        float wid = rect.width;
        float hei = rect.height;

        float left = vec.x - wid / 2;
        float right = vec.x + wid / 2;
        float top = vec.y + hei / 2;
        float bottom = vec.y - hei / 2;

        return left <= x && x <= right && bottom <= y && y <= top;
    }

    public void setActive(bool v){
        this.active = v;

        if(v){
           tileText.enabled = true;
           tileImage.enabled = true;
        }
        else{
            tileText.enabled = false;
            tileImage.enabled = false;
        }
    }

    public void setColor(int colorInd){
        switch (colorInd)
        {
            case 0:
                color = new Color32(255, 50, 50, 255);
                break;
            case 1:
                color = new Color32(118, 111, 255, 255);
                break;
            case 2:
                color = new Color32(253, 255, 151, 255);
                break;
            case 3:
                color = new Color32(118, 255, 111, 255);
                break;
            case 4:
                color = new Color32(240, 60, 252, 255);
                break;
            case 5:
                color = new Color32(23, 255, 255, 255);
                break;
           case 6:
                color = new Color32(240, 138, 10, 255);
                break;
           case 7:
                color = new Color32(255, 255, 240, 255);
                break;
        }
    }
}
