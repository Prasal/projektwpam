﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallService : MonoBehaviour
{
    private Game game;
    private VanishService vanishService;
	 
    void Start ()
    {
        game = FindObjectOfType<Game>();
        vanishService = FindObjectOfType<VanishService>();
    }
    
    public void performFallAfterMove()
    {
        performFall();
        StartCoroutine(waiterVanish());
    }
    
    public void performFallOfDifferentReason()
    {
        bool hasSomethingFallen = performFall();

        if (hasSomethingFallen)
        {
            StartCoroutine(waiterVanish());
        }
        else
        {
            game.checkIfNoMoves();
        }
    }

    public bool performFall()
    {
        bool hasSomethingFallen = false;
        
        for (int currRow = 1; currRow < 6; currRow++)
        {
            Tile[,] tiles = game.getTiles();
            Dictionary<Tile,Tile> tempMapOfHeights = new Dictionary<Tile,Tile>();

            for (int col = 0; col < 4; col++)
            {
                Tile t = tiles[currRow, col];
 
                if (!t.active)
                {
                    continue;
                }

                int i = currRow - 1;
                int heighOfFall = 0;
				
                while (i >= 0)
                {
                    if (!tiles[i, t.col].active)
                    {
                        heighOfFall++;
                    }
					
                    i--;
                }

                if (heighOfFall == 0)
                {
                    continue;
                }
            
                Tile newPositionAfterFall = tiles[currRow - heighOfFall, t.col];
                tempMapOfHeights.Add(t, newPositionAfterFall);
            }

            if (!hasSomethingFallen && tempMapOfHeights.Count > 0)
            {
                hasSomethingFallen = true;
            }
        
            foreach (var x in tempMapOfHeights)
            {
                Tile tile1 = x.Key;
                Tile tile2 = x.Value;
                game.replaceTilesOnMap(tile1, tile2);
            }
        }

        return hasSomethingFallen;
    }
    
    IEnumerator waiterVanish()
    {
        yield return new WaitForSeconds(Game.TIME_OF_PAUSE);
        List<Tile> vanishedTiles = vanishService.determineTilesToBeDeleted();
        Tile vanishedLetter = determineVanishedLetter(vanishedTiles);
        
        int numberOfPointsToAdd = (int) Math.Pow(vanishedTiles.Count, 2);
        
        if (vanishedLetter != null)
        {
            game.activatedCollectedLetter(vanishedLetter.color);
            game.isLetterToBeSpawned = true;
            numberOfPointsToAdd += 20;
        }

        game.score += numberOfPointsToAdd;
        
        foreach (Tile tile in vanishedTiles)
        {
            tile.letter = null;
        }
        
        if (vanishedTiles.Count > 0)
        {
            StartCoroutine(waiterFallOfDifferentReason());
        }
        else
        {
            game.checkIfNoMoves();
        }
    }
    private Tile determineVanishedLetter(List<Tile> vanishedTiles)
    {
        foreach (Tile vanishedTile in vanishedTiles)
        {
            if (vanishedTile.letter != null)
            {
                return vanishedTile;
            }
        }
        return null;
    }
    
    public IEnumerator waiterFallOfDifferentReason()
    {
        yield return new WaitForSeconds(Game.TIME_OF_PAUSE);
        performFallOfDifferentReason();
    }
    
    public IEnumerator waiterFallAfterMoves()
    {
        yield return new WaitForSeconds(Game.TIME_OF_PAUSE);
        performFallAfterMove();
    }
}