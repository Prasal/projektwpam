﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectorService : MonoBehaviour {

    private SwypeService swypeService;
    private Game game;

    private bool keyDown;
    private Tile firstChosenTile;
    private Tile secondChosenTile;

    void Start () {
        swypeService = FindObjectOfType<SwypeService>();
        game = FindObjectOfType<Game>();
    }
	
    void Update () {
        detectMovement();
    }

    private void detectMovement(){
        if (game.isScreenLocked){
            return;
        }
        
        Tile[,] tiles = game.getTiles();
        Tile foundTile = null;
        float xTouch = Input.mousePosition.x;
        float yTouch = Input.mousePosition.y;

        foreach (Tile t in tiles){
            if (t.isItYou(xTouch, yTouch)){
                foundTile = t;
                break;
            }
        }

        if (foundTile == null){
            return;
        }

        if (Input.GetMouseButtonDown(0) && foundTile.active){
            keyDown = true;
            firstChosenTile = foundTile;
        }
        
        if (firstChosenTile == null){
            return;
        }

        if (firstChosenTile != null && !foundTile.active && firstChosenTile.row < foundTile.row
            || firstChosenTile.row != foundTile.row && firstChosenTile.col != foundTile.col){
            keyDown = false;

            firstChosenTile = null;
            secondChosenTile = null;
        }

        if (keyDown && foundTile != firstChosenTile && !(!foundTile.active && firstChosenTile.row < foundTile.row)){
            secondChosenTile = foundTile;
            keyDown = false;

            swypeService.setSwype(firstChosenTile, secondChosenTile);
            firstChosenTile = null;
            secondChosenTile = null;
        }
    }
}