﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MenuOption3 : MonoBehaviour, IPointerClickHandler
{
	private MenuView menuView;

	void Awake()
	{
		menuView = FindObjectOfType<MenuView>();
	}

	public void OnPointerClick(PointerEventData pointerEventData)
	{
		// CANCEL
		menuView.hideMenu();
	}
}
