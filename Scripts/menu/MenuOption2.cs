﻿using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuOption2 : MonoBehaviour, IPointerClickHandler
{
	private MenuView menuView;

	void Awake()
	{
		menuView = FindObjectOfType<MenuView>();
	}

	public void OnPointerClick(PointerEventData pointerEventData)
	{
		// QUIT THE GAME
		menuView.hideMenu();
		Application.Quit();
	}
}
