﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MenuListener : MonoBehaviour, IPointerClickHandler
{
    private MenuView menuView;

    void Awake()
    {
        menuView = FindObjectOfType<MenuView>();
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
       menuView.showMenu(MenuView.MenuMode.Normal);
    }
}
