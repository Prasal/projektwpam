﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuView : MonoBehaviour {

    private List<MaskableGraphic> listOfUIelems = new List<MaskableGraphic>();
    private Game game;
    private Text menuTitle;
    public enum MenuMode {Normal, Win, Lose};

    private MenuMode currMode;
    
    void Awake()
    {		
        game = FindObjectOfType<Game>();
        menuTitle = transform.Find("MenuTitle").GetComponent<Text>();
            
        listOfUIelems.Add( GetComponentInChildren<Image>() );
        listOfUIelems.Add( GetComponentInChildren<Text>() );
        listOfUIelems.Add( transform.Find("Image").GetComponent<Image>() );
        
        listOfUIelems.Add( menuTitle );
        listOfUIelems.Add( transform.Find("MenuOption1").GetComponent<Text>() );
        listOfUIelems.Add( transform.Find("MenuOption2").GetComponent<Text>() );
        listOfUIelems.Add( transform.Find("MenuOption3").GetComponent<Text>() );
        
        hideMenu();
    }

    public void showMenu(MenuMode menuMode)
    {
        currMode = menuMode;
        
        foreach (var listOfUIelem in listOfUIelems)
        {
            listOfUIelem.enabled = true;
        }
        
        game.isScreenLocked = true;
        
        if (menuMode == MenuMode.Normal)
        {
            menuTitle.text = "Menu";
        }
        else if (menuMode == MenuMode.Win)
        {
            menuTitle.text = "You won!";
        }
        else if (menuMode == MenuMode.Lose)
        {
            menuTitle.text = "You lose";
        }
    }
    
    public void hideMenu()
    {       
        foreach (var listOfUIelem in listOfUIelems)
        {
            listOfUIelem.enabled = false;
        }
        
        if (currMode == MenuMode.Normal)
        {
            game.isScreenLocked = false;
        }
    }
}