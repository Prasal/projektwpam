﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MenuOption1 : MonoBehaviour, IPointerClickHandler
{
	private MenuView menuView;
	private Game game;

	void Awake()
	{
		menuView = FindObjectOfType<MenuView>();
		game = FindObjectOfType<Game>();
	}

	public void OnPointerClick(PointerEventData pointerEventData)
	{
		// RESTART
		game.newGame();
		menuView.hideMenu();
	}
}
