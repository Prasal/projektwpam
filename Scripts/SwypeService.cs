﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwypeService : MonoBehaviour {

    private Game game;
    private FallService fallService;

    void Start () {
        game = FindObjectOfType<Game>();
        fallService = FindObjectOfType<FallService>();
    }

    public void setSwype(Tile tile1, Tile tile2)
    {
        game.isScreenLocked = true;
        game.replaceTilesOnMap(tile1, tile2);
        game.moves--;
        StartCoroutine( fallService.waiterFallAfterMoves() );
    }
}