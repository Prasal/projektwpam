﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VanishService : MonoBehaviour
{
	private Game game;
	 
	void Start ()
	{
		game = FindObjectOfType<Game>();
	}

	public List<Tile> determineTilesToBeDeleted()
	{
		List<Tile> tilesToBeDeleted = new List<Tile>();
		List<Tile> listOfVisited = new List<Tile>();

		foreach (Tile tile in game.getTiles())
		{
			List<Tile> portion = getTilesToBeDeleted(listOfVisited, tile);
			tilesToBeDeleted.AddRange(portion);
		}
		
		foreach (Tile tile in tilesToBeDeleted)
		{
			tile.setActive(false);
		}

		return tilesToBeDeleted;
	}

	private List<Tile> getTilesToBeDeleted(List<Tile> listOfVisited, Tile tile){
		Tile[,] tiles  = game.getTiles();
		Queue<Tile> tilesToCheck = new Queue<Tile>();
		List<Tile> localTilesToBeDeleted = new List<Tile>();
		
		Color32 searchedColor = tile.color;
		tilesToCheck.Enqueue(tile);
		localTilesToBeDeleted.Add(tile);
		listOfVisited.Add(tile);
		
		while (tilesToCheck.Count > 0){
			Tile t = tilesToCheck.Dequeue();
			
			int row = t.row;
			int col = t.col;

			validateAndAdd(tiles, searchedColor, tilesToCheck, localTilesToBeDeleted, listOfVisited,row - 1, col);
			validateAndAdd(tiles, searchedColor, tilesToCheck, localTilesToBeDeleted, listOfVisited,row + 1, col);
			validateAndAdd(tiles, searchedColor, tilesToCheck, localTilesToBeDeleted, listOfVisited,row, col - 1);
			validateAndAdd(tiles, searchedColor, tilesToCheck, localTilesToBeDeleted, listOfVisited,row, col + 1);
		}

		if (localTilesToBeDeleted.Count >= 4)
		{
			return localTilesToBeDeleted;	
		}

		return new List<Tile>();
	}

	private void validateAndAdd(Tile[,] tiles, Color32 searchedColor, Queue<Tile> tilesToCheck,
		List<Tile> tilesToBeDeleted, List<Tile> listOfVisited, int i, int j)
	{
		if (i < 0 || i > 5 || j < 0 || j > 3){
			return;
		}

		Tile t = tiles[i, j];
		
		if (!listOfVisited.Contains(t) && t.active && t.color.Equals(searchedColor)){
			tilesToCheck.Enqueue(t);
			tilesToBeDeleted.Add(t);
			listOfVisited.Add(t);
		}
	}

}
