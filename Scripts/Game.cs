﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Game : MonoBehaviour
{
    public static float TIME_OF_PAUSE = 0.2f;
    private string[] LETTERS = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "Y", "X", "Z" };
    private ScoreText scoreText;
    private MovesText movesText;
    private FallService fallService;
    private MenuView menuView;

    private Tile[,] tiles = new Tile[6, 4];
    private CollectedTile[] collectedLetters = new CollectedTile[26];
    private int currLetterIndex = 0;
    public int score;
    public int moves = 4;

    public bool isScreenLocked;
    public bool isLetterToBeSpawned;

    void Start () {
        scoreText = FindObjectOfType<ScoreText>();
        movesText = FindObjectOfType<MovesText>();
        fallService = FindObjectOfType<FallService>();
        menuView = FindObjectOfType<MenuView>();
        newGame();
    }
	
    void Update () {
        scoreText.setText(score);
        movesText.setText(moves);
    }

    public void newGame()
    {
        isScreenLocked = false;
        
        Tile[] initialStateOfTiles = FindObjectsOfType<Tile>();

        foreach (Tile t in initialStateOfTiles){
            t.letter = null;
        }
        
        CollectedTile[] initialStateOfCollectedTiles = FindObjectsOfType<CollectedTile>();
        
        foreach (CollectedTile t in initialStateOfCollectedTiles){
            collectedLetters[t.index] = t;
            t.setActive(false);
        }

        moves = 4;
        score = 0;
        currLetterIndex = 0;

        int rndRow = Random.Range(0, 3);
        int rndCol = Random.Range(0, 4);

        foreach (Tile t in initialStateOfTiles){
            tiles[t.row, t.col] = t;

            if (t.row > 2){
                t.setActive(false);
            }
            else{
                t.setActive(true);
            }

            if (t.row == rndRow && t.col == rndCol){
                t.letter = LETTERS[currLetterIndex];
            }

            int rndColor = Random.Range(0, 8);
            t.setColor(rndColor);
        }
    }
   

    public void replaceTilesOnMap(Tile tile1, Tile tile2){
        int t1Row = tile1.row;
        int t2Row = tile2.row;
        int t1Col = tile1.col;
        int t2Col = tile2.col;

        tiles[t1Row, t1Col] = tile2;
        tiles[t2Row, t2Col] = tile1;

        tile1.row = t2Row;
        tile1.col = t2Col;

        tile2.row = t1Row;
        tile2.col = t1Col;

        Vector3 vec1 = tile1.transform.position;
        Vector3 vec2 = tile2.transform.position;

        tile1.transform.position = vec2;
        tile2.transform.position = vec1;
    }

    public Tile[,] getTiles()
    {
        return tiles;
    }

    public void activatedCollectedLetter(Color32 vanishedLetterColor)
    {
        collectedLetters[currLetterIndex].setActive(true);
        collectedLetters[currLetterIndex].setColor(vanishedLetterColor);
    }

    public void checkIfNoMoves()
    {
        if(isLetterToBeSpawned  && currLetterIndex == 25)
        {
            win();
            return;
        }

        if(isLetterToBeSpawned)
        {
            spawnOfLetter();
            isLetterToBeSpawned = false;
        }
        else if (moves == 0)
        {
            moves = 4;
            spawnOfTilesGeneral();
        }
        else
        {
            isScreenLocked = false;
        }
    }

    private void spawnOfTilesGeneral()
    {
        bool isLose = !isSpawnPossible();

        spawnOfTiles();

        if (isLose)
        {
            lose();
            return;
        }

        StartCoroutine(fallService.waiterFallAfterMoves());
    }
   
    private void spawnOfLetter()
    {
        int rndCol;

        do
        {
            rndCol = Random.Range(0, 4);
        } while (tiles[5, rndCol].active);

        Tile newTileWithLetter = tiles[5, rndCol];
       
        newTileWithLetter.setActive(true);
        newTileWithLetter.setColor( Random.Range(0, 8) );
        newTileWithLetter.letter = LETTERS[++currLetterIndex];
       
        StartCoroutine(fallService.waiterFallAfterMoves());
    }
    
    private void spawnOfTiles()
    {
        for (int i = 0; i < 4; i++)
        {
            Tile t = tiles[5, i];
            
            if (!t.active)
            {
                t.setActive(true);
                t.setColor( Random.Range(0, 8) );   
            }
        }
    }
    
    private bool isSpawnPossible()
    {
        for (int i = 0; i < 4; i++)
        {
            if (tiles[5, i].active)
            {
                return false;
            }
        }

        return true;
    }

    private void win()
    {
        isLetterToBeSpawned = false;
        isLetterToBeSpawned = false;
        menuView.showMenu(MenuView.MenuMode.Win);
    }
    
    private void lose()
    {
        isLetterToBeSpawned = false;
        isLetterToBeSpawned = false;
        menuView.showMenu(MenuView.MenuMode.Lose);
    }
}