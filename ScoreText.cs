﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour {

    private Text text;

    private void Awake()
    {
        text = GetComponentInChildren<Text>();
    }

    public void setText(int score)
    {
        text.text = "SCORE: " + score.ToString();
    }
}
